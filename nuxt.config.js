module.exports = {
    modules: [
        '@nuxtjs/axios'
    ],
    plugins: [
        { src: '~plugins/persisted-state', ssr: false },
        { src: '~plugins/auth', ssr: true },
        { src: '~plugins/fontawesome', ssr: true },
        { src: '~plugins/vee-validate', ssr: true },
        { src: '~plugins/filters', ssr: true },
        { src: '~plugins/i18n', ssr: true }
    ],
    head: {
        title: 'Comparateur',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Comparateur des élements du jeu StarCitizen' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Oxygen:300,400,700' }
        ]
    },
    loading: {
        color: '#ff5638',
        height: '4px'
    },
    css: [
        'normalize-scss'
    ],
    build: {
        extend( config, { isDev, isClient } ) {
            /*
            ** Run ESLint on save
            */
            if ( isDev && isClient ) {
                config.module.rules.push( {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                } );
            }

            config.node = {
                fs: 'empty'
            };
        },
        /*
		** SCSS variables / mixins
		*/
        styleResources: {
            scss: './assets/scss/main.scss',
            options: {
                // See https://github.com/yenshih/style-resources-loader#options
                // Except `patterns` property
            }
        }
    },
    axios: {
        baseURL: 'http://localhost:7005'
        // requestInterceptor: (config, {store}) => {
        //     config.headers.common['Authorization'] = 'Bearer ' + store.state.token
        //     return config
        // }
    }
};
