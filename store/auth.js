export const state = () => ({
    authenticated: false,
    token: null
});

export const mutations = {
    setToken( state, token ) {
        state.token = token;
    },
    setAuthenticated( state, boolean ) {
        state.authenticated = boolean;
    }
};
