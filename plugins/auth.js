import Vue from 'vue';

export default async ( { store, redirect } ) => {

    const auth = {
        install( Vue, options ) {
            Vue.prototype.$auth = {
                logout() {
                    store.commit( 'auth/setToken', null );
                    store.commit( 'auth/setAuthenticated', false );
                    return redirect('/');
                }
            };
        }
    };

    Vue.use( auth );

}


