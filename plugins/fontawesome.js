import { library, dom } from '@fortawesome/fontawesome-svg-core'

import { faChevronLeft } from '@fortawesome/pro-light-svg-icons';
import { faTimesCircle } from '@fortawesome/pro-light-svg-icons';

library.add(
    faChevronLeft,
    faTimesCircle
);

dom.watch();
