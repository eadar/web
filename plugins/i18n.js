import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use( VueI18n );

export default async ( { app, store } ) => {

    /**
     * Only import the selected language translation file
     * @returns {Promise<void>}
     */
    const translations = async function () {
        let test = {};
        test[store.state.lang.locale] = require( `~/locales/${store.state.lang.locale}/translations.json` );
        return test;
    };

    app.i18n = new VueI18n( {
        locale: store.state.lang.locale,
        fallbackLocale: 'en_US',
        messages: await translations()
    } );
}
