import Vue from 'vue';
import { Validator, install } from 'vee-validate';

Vue.use( install );

Validator.extend( 'required', {
    getMessage( field, params, data ) {
        return (data && data.message) || 'Le champ est requis';
    },
    validate( value ) {
        return !!value;
    }
} );

Validator.extend( 'uid', {
    getMessage( field, params, data ) {
        return (data && data.message) || 'L\identifiant est incorrect';
    },
    validate( value ) {
        return !!value.match( /^[a-z\d][a-z\d-]{2,}[a-z\d]$/ );
    }
} );

Validator.extend( 'password', {
    getMessage( field, params, data ) {
        return (data && data.message) || 'Le mot de passe est incorrect';
    },
    validate( value ) {
        return !!value.match( /^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/ );
    }
} );

// export default async ( { app, store } ) => {
//
//     console.log(app.)
// }

// const password = {
//     getMessage( field, params, data ) {
//         return (data && data.message) || 'Le mot de passe est incorrect';
//     },
//     validate( value ) {
//         return new Promise( resolve => {
//             resolve( {
//                 valid: value === 'trigger' ? false : !!value,
//                 data: value !== 'trigger' ? undefined : { message: 'Not this value' }
//             } );
//         } );
//     }
// };
